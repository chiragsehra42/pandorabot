### Requirements
    Python = 2.x.x
    Flask
    Aiml
    pip

## Running the demo

1. Install the required packages.
    ```pip install -r requirements.txt```

3. Run the python server.
    ```python app.py```

4. Open **http://127.0.0.1:5000** in your browser.
